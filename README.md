
 # Automatic Tuning of Denoising Algorithms Parameters without Ground Truth

This is the GitLab repository of the article : "Automatic Tuning of Denoising Algorithms Parameters without Ground Truth".
The article can be found on [arXiv](http://arxiv.org/abs/2401.09817), [Hall](https://hal.science/hal-04344047) and [IEEE Xplore](https://ieeexplore.ieee.org/document/10400797).
Please cite our article if you are using our work.


## Requirements
The code was run on [PyTorch 1.12.0](https://pytorch.org/get-started/previous-versions/)

## Replicate results
First, run "config.py" to create the proper dir architecture.

To replicate results fig.2 (main result of the article), you must run the files "fit_XXX.py" (XXX = "N2N", "R2R", etc.) and "inference_R2R.py" to fill the "_megacomparison" result folder. 
Then, run "fig2.py" to replicate fig.2, and "fig1.py" to replicate fig.1. 

To replicate fig.3, run "fit_N2N_supervised_poisson.py", then "fig3.py".

## A quick note on NaNs 
Upon running the code, you will note that sometimes, the loss is NaN.  We emphasize that this issue is caused by the denoiser we chose, and not our method in itself.  

This is due to the eigendecomposition of the Hamiltonian in DeQuIP. The gradient of the eigendecomposition being proportionnal to $\frac{1}{min_{i \neq j}|\lambda_i - \lambda_j|}$, it is unstable when there are close eigenvalues, and produces NaNs when there are repeating eigenvalues. This can be dealt with in 2 ways : 
 * Ignoring gradient that contains NaNs. This is not useful for ${\hat{\mathbf{x}}}^{\mathrm{N2N}}$ and $\mathbf{x}^*$ because the optimization is deterministic. For the other methods, since $\mathbf{z}$ are drawn at each iteration, it should work.
 *  Pertubating the Hamiltonian with small noise until there are no repeating eigenvalues. We found out that it does not modify the eigenvectors too much, so the denoising is not affected that much.

If you are to use a denoiser with stable gradient, this issue will not arise (or if don't use gradient descent based optimization).