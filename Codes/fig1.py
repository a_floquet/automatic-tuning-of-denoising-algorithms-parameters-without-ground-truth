# -*- coding: utf-8 -*-
"""
Created on Fri Jul 21 16:59:59 2023

@author: afloquet
"""

#%%
import os
from os import listdir
from os.path import join
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import container
from matplotlib.lines import Line2D
import noiselib
from utils import compute_PSNR

def no_axis(ax):
    if ax.ndim == 1 : 
        for a in ax :
            a.set_yticklabels([])
            a.set_xticklabels([])
            a.set_xticks([])
            a.set_yticks([])
    if ax.ndim ==2 : 
        for row in ax :
            for a in row :
                a.set_yticklabels([])
                a.set_xticklabels([])
                a.set_xticks([])
                a.set_yticks([])

#%%
motherdir = '../../Results/DeQuIP_fitting/_megacomparison'

imlist = np.zeros((3,7,180,180))
imgs_code = [('077', '17', '100'), ('086', '22', '100'), ('106', '30', '100')]

for i, code in enumerate(imgs_code) :
    
    supdir = join(motherdir, 'supervised', f'test_{code[0]}_psnr{code[1]}')
    n2ndir = join(motherdir, 'N2N', f'test_{code[0]}_psnr{code[1]}')
    nacdir = join(motherdir, 'NaC', f'test_{code[0]}_psnr{code[1]}')
    nr2ndir = join(motherdir, 'Nr2N', f'test_{code[0]}_psnr{code[1]}')
    r2rdir = join(motherdir, 'R2R', f'test_{code[0]}_psnr{code[1]}')
    
    imlist[i,0,...] = np.load(join(supdir, 'noisy_img.npy'))
    imlist[i,1,...] = np.load(join(nr2ndir, 'inference_final.npy'))
    imlist[i,2,...] = np.load(join(nacdir, 'inference_final.npy'))
    imlist[i,3,...] = np.load(join(r2rdir, 'inference_final.npy'))
    imlist[i,4,...] = np.load(join(n2ndir, 'out_100.npy'))
    imlist[i,5,...] = np.load(join(supdir, f'out_{code[2]}.npy'))
    imlist[i,6,...] = np.load(join(supdir, 'clean_img.npy'))
    
psnr_list = np.zeros((3,6))
for i in range(3): 
    psnr_list[i,0] = int(imgs_code[i][1])
    for j in range(5):
        psnr_list[i,j+1] = round(compute_PSNR(imlist[i,6,...], imlist[i,j+1,...]),3)

label_list = [r'$\mathbf{y}$',
              r'$\hat{\mathbf{x}}^{\mathrm{Nr2N}}$',
              r'$\hat{\mathbf{x}}^{\mathrm{NaC}}$',
              r'$\hat{\mathbf{x}}^{\mathrm{R2R}}$',
              r'$\hat{\mathbf{x}}^{\mathrm{N2N}}$',
              r'$\mathbf{x}^{*}$',
              r'$\mathbf{x}$']
        
fig, ax = plt.subplots(3,7, dpi=500)
no_axis(ax)
for i in range(3):
    for j in range(7):
        ax[i,j].imshow(imlist[i,j,...], cmap='gray')
        if   j == 0 : ax[i,j].set_title(f'{int(psnr_list[i,j])}dB', size=8)
        elif j != 6 : ax[i,j].set_title(f'{psnr_list[i,j]}dB', size=8)
        if   i == 2 : ax[i,j].set(xlabel=label_list[j])
        
fig.tight_layout()
fig.subplots_adjust(hspace=-0.4)
plt.show()
        
