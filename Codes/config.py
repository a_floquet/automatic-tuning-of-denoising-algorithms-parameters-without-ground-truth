# -*- coding: utf-8 -*-
from os import mkdir
from os.path import isdir

if not isdir("../Results")                                          : mkdir("../Results")
if not isdir("../Results/DeQuIP_fitting")                           : mkdir("../Results/DeQuIP_fitting")
if not isdir("../Results/DeQuIP_fitting/_megacomparison")           : mkdir("../Results/DeQuIP_fitting/_megacomparison")
if not isdir("../Results/DeQuIP_fitting/_megacomparison/N2N")       : mkdir("../Results/DeQuIP_fitting/_megacomparison/N2N")
if not isdir("../Results/DeQuIP_fitting/_megacomparison/R2R")       : mkdir("../Results/DeQuIP_fitting/_megacomparison/R2R")
if not isdir("../Results/DeQuIP_fitting/_megacomparison/NaC")       : mkdir("../Results/DeQuIP_fitting/_megacomparison/Nac")
if not isdir("../Results/DeQuIP_fitting/_megacomparison/Nr2N")      : mkdir("../Results/DeQuIP_fitting/_megacomparison/Nr2N")
if not isdir("../Results/DeQuIP_fitting/_megacomparison/SC_metric") : mkdir("../Results/DeQuIP_fitting/_megacomparison/SC_metric")
if not isdir("../Results/DeQuIP_fitting/N2N_sup_comparison")        : mkdir("../Results/DeQuIP_fitting/_megacomparison/Nr2N")
