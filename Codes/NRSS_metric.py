# -*- coding: utf-8 -*-
"""
Created on Thu Dec 14 15:20:07 2023

@author: afloquet
"""

import torch.nn as nn
import torch
from math import ceil


class SC_metric(nn.Module):
    
    def __init__(self):
        super(SC_metric, self).__init__()
        
    def patchization(self,x):

        # Compute number of patches and create to-be-filled tensor
        N,C,H,W = x.shape
        P_h = ceil(1 + H - 7) # number of patches along height
        P_w = ceil(1 + W - 7) # number of patches along width 
        patches = torch.zeros((N, P_h*P_w, C, 7, 7)).type(x.type())
    
        # Loop over all patches
        P_idx = 0 # idex of patch ij in the dimension P
        for i in range(P_h):
            for j in range(P_w):
                # Compute patch index limits for the 4 possible cases
                if i != P_h-1 and j != P_w -1: # patch is fully inside x
                    i_low, i_up = i, 7 + i
                    j_low, j_up = j, 7 + j
                elif i == P_h-1 and j != P_w-1: # patch overflows on the bottom border of x
                    i_low, i_up = H - 7, H
                    j_low, j_up = j, 7 + j
                elif i != P_h-1 and j == P_w-1: # patch overflows on the right border of x
                    i_low, i_up = i, 7 + i
                    j_low, j_up = W - 7, W
                else : # patch overflows on the bottom right corner of x
                    i_low, i_up = H - 7, H
                    j_low, j_up = W - 7, W
                # Fill final tensor with computed patch    
                p_ij = x[:, :, i_low:i_up, j_low:j_up]
                patches[:,P_idx, :, :, :] = p_ij
                P_idx +=1
                                   
        return patches
    
    
    def forward(self, noisy, denoised, c = 1e-10):
        
        # Loop over all pixels (no loop used, rather patchization)
        patch_noisy    = self.patchization(noisy)
        patch_denoised = self.patchization(denoised)
        patch_MNI      = patch_noisy - patch_denoised
        
        # Compute means & std
        mean_noisy    = patch_noisy.mean(dim=(-1,-2)).unsqueeze(-1).unsqueeze(-1)
        mean_denoised = patch_denoised.mean(dim=(-1,-2)).unsqueeze(-1).unsqueeze(-1)
        mean_MNI      = patch_MNI.mean(dim=(-1,-2)).unsqueeze(-1).unsqueeze(-1)
        
        std_noisy     = patch_noisy.std(dim=(-1,-2)).unsqueeze(-1).unsqueeze(-1)
        std_denoised  = patch_denoised.std(dim=(-1,-2)).unsqueeze(-1).unsqueeze(-1)
        std_MNI       = patch_MNI.std(dim=(-1,-2)).unsqueeze(-1).unsqueeze(-1)
        
        std_noisy_MNI      = (torch.sum((patch_noisy - mean_noisy) * (patch_MNI      - mean_MNI)     , dim=(-1,-2)) / 48).squeeze()
        std_noisy_denoised = (torch.sum((patch_noisy - mean_noisy) * (patch_denoised - mean_denoised), dim=(-1,-2)) / 48).squeeze()
        
        N = (std_noisy_MNI      + c) / (std_noisy.squeeze()*std_MNI.squeeze()      + c)
        P = (std_noisy_denoised + c) / (std_noisy.squeeze()*std_denoised.squeeze() + c)
        
        covariance = torch.mean((N - N.mean()) * (P - P.mean()))
        e = (covariance + c) / (N.std() * P.std() + c)
       
        return e