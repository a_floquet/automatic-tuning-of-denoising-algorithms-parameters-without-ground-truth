# -*- coding: utf-8 -*-
"""
Created on Thu Jul  6 11:15:50 2023

@author: afloquet
"""

#%% Libs
from os import mkdir
from os import listdir
from os.path import join
from os.path import isfile
import numpy as np
import torch
import matplotlib.pyplot as plt

import utils
import noiselib
from DeQuIP_pytorch import DeQuIP_mother

#%% Settings
imgdir = '../Data/Fitting_images'
datadir   = '../Results/DeQuIP_fitting/_megacomparison/R2R'
dir_list = listdir(datadir)
K = 50

# DeQuIP settings
patch_size = 7
overlap = 'useless'
patch_stride = 1
search_window_size = 2

#R2R settings
alpha = 0.5

# Noise settings
noise_type = "gaussian"
noise_metric = "psnr"


np_dtype = np.float32
torch_dtype = torch.cuda.FloatTensor
#%% Init
DeQuIP = DeQuIP_mother(patch_size, overlap, search_window_size, patch_stride = patch_stride).type(torch_dtype)

#%% Script
for dir_path in dir_list :
    # Get y
    im_name = dir_path[:8]
    strength = int(dir_path[-2:])
    y_np = np.load(join(imgdir, f'noisy_{strength}dB', f'{im_name}.npy'))
    
    savepath = join(datadir, dir_path,'inference_final.npy')
    if not isfile(savepath) : 
    
        y = torch.from_numpy(y_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)
        
        params = torch.load(join(datadir, dir_path, 'params_final.pt'))
        params_list = [p.clone().detach() for p in params]
        p, planck, c1, c2 = params_list
        
        inf = np.zeros(y_np.shape)
        
        with torch.no_grad() : 
            for k in range(K):
                _, n  = noiselib.add_noise(y, noise_type, strength, noise_metric, dtype = torch_dtype.dtype, d = 255., keep_noise=True)
                z_k = y + alpha * n
                out = DeQuIP(y, p, planck, c1, c2)
                out_np = out.detach().squeeze().cpu().numpy()
                inf += out_np/K
        
        np.save(savepath, inf)
