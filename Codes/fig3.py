# -*- coding: utf-8 -*-
"""
Created on Tue Jul 18 19:19:29 2023

@author: afloquet
"""

#%% Libs
import os
import numpy as np
import matplotlib.pyplot as plt
import noiselib
import utils


def no_axis(ax):
    if ax.ndim == 1 : 
        for a in ax :
            a.set_yticklabels([])
            a.set_xticklabels([])
            a.set_xticks([])
            a.set_yticks([])
    if ax.ndim ==2 : 
        for row in ax :
            for a in row :
                a.set_yticklabels([])
                a.set_xticklabels([])
                a.set_xticks([])
                a.set_yticks([])

def norm(x):
    return  (x - x.min() + 1) / (x.max() - x.min() + 1) * 255.
                
#%% Script 
dirpath = '../../Results/DeQuIP_fitting/N2N_sup_comparison/test_296_poisson_lamb200_stride1'
im_path = '../../Data/Fitting_images/clean/test_296.png'
normalize = False

x = plt.imread(im_path) * 255.
y = noiselib.apply_poisson(x, 100)

n2n = np.load(os.path.join(dirpath, 'out_n2n_100.npy'))
sup = np.load(os.path.join(dirpath, 'out_sup_100.npy'))

if normalize : 
    y   = norm(y)
    n2n = norm(n2n)
    sup = norm(sup)
    

psnr_n2n = round(utils.compute_PSNR(n2n, x, d=255.),3)
psnr_sup = round(utils.compute_PSNR(sup, x, d=255.),3)
psnr_y   = int(utils.compute_PSNR(y, x, d=255.))


# 1 x 4 figure
fig, ax = plt.subplots(1, 4, dpi=500)
no_axis(ax)

ax[0].imshow(y, cmap='gray')
ax[0].set(xlabel=r'$\mathbf{y}$')
ax[0].set_title(f'{psnr_y}dB')

ax[1].imshow(n2n, cmap='gray')
ax[1].set(xlabel=r'$\hat{\mathbf{x}}^{\mathrm{N2N}}$')
ax[1].set_title(f'{psnr_n2n}dB')

ax[2].imshow(sup, cmap='gray')
ax[2].set(xlabel=r'$\mathbf{x}^{*}$')
ax[2].set_title(f'{psnr_sup}dB')

ax[3].imshow(x, cmap='gray')
ax[3].set(xlabel=r'$\mathbf{x}$')

fig.tight_layout()
plt.show()

# 2 x 2 figure
# fig, ax = plt.subplots(2, 2, dpi=500)
# no_axis(ax)

# ax[0,0].imshow(y, cmap='gray')
# ax[0,0].set(xlabel='noisy')
# ax[0,0].set_title(f'{psnr_y}dB')

# ax[0,1].imshow(x, cmap='gray')
# ax[0,1].set(xlabel='clean')

# ax[1,0].imshow(n2n, cmap='gray')
# ax[1,0].set(xlabel=r'$\hat{\mathcal{L}}^{N2N}$')
# ax[1,0].set_title(f'{psnr_n2n}dB')

# ax[1,1].imshow(sup, cmap='gray')
# ax[1,1].set(xlabel=r'$\mathcal{L}^{*}$')
# ax[1,1].set_title(f'{psnr_sup}dB')

# fig.tight_layout()
# fig.subplots_adjust(wspace=-0.6)
# plt.show()