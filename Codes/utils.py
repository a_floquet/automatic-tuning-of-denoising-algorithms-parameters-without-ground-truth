# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 08:52:27 2022

@author: afloquet

Utils for the  PPNet + DeQuIP framework
"""

#%% Imports
from random import uniform
import os
from PIL import Image
import numpy as np
import torch
import torch.nn as nn
import torch.utils.data as data
from torchvision import transforms as T
from math import floor

#%% Dataset loader
def GetAllFiles(path):
    listOfFiles = list()
    for (dirpath, dirnames, filenames) in os.walk(path):
        listOfFiles += [os.path.join(dirpath, file) for file in filenames]
    return listOfFiles
    
def get_optimizer(name, model, lr, params = False):
    """ return chosen optimizer """
    if name in ("adam","Adam","ADAM") :
        betas = params if params else (0.9,0.999)
        return torch.optim.Adam(model.parameters(), lr=lr, betas=betas)
    if name in ("adadelta", "Adadleta", "ADADELTA"):
        rho = params if params else 0.9
        return torch.optim.Adadelta(model.parameters(), lr=lr, rho=rho)
    if name in ("adagrad", "Adagrad", "ADAGRAD"):
        return torch.optim.Adagrad(model.parameters(), lr=lr)
    if name in ("adamw", "adamW", "Adamw", "AdamW", "ADAMW"):
        betas = params if params else (0.9,0.999)
        return torch.optim.AdamW(model.parameters(), lr=lr, betas=betas)
    if name in ("adamax", "Adamax", "ADAMAX"):
        betas = params if params else (0.9,0.999)
        return torch.optim.Adamax(model.parameters(), lr=lr, betas=betas)
    if name in ("asgd", "ASGD"):
        lambd, alpha = params if params else 0.0001, 0.75
        return torch.optim.ASGD(model.parameters(), lr=lr, lambd=lambd, alpha=alpha)
    if name in ("LBFGS"):
        max_iter = params if params else 20
        return torch.optim.LBFGS(model.parameters(), lr=lr, max_iter=max_iter)
    if name in ("nadam","NAdam","NADAM") :
        betas = params if params else (0.9,0.999)
        return torch.optim.NAdam(model.parameters(), lr=lr, betas=betas)
    if name in ("radam","RAdam","RADAM") :
        betas = params if params else (0.9,0.999)
        return torch.optim.RAdam(model.parameters(), lr=lr, betas=betas)
    if name in ("rms","RMS","rmsprop", "RMSprop") :
        alpha = params if params else 0.99
        return torch.optim.RMSprop(model.parameters(), lr=lr, alpha=alpha)
    if name in ("rprop","Rprop","RPROP") :
        etas = params if params else (0.5,1.2)
        return torch.optim.Rprop(model.parameters(), lr=lr, etas=etas)
    if name in ("sgd", "SGD") :
        return torch.optim.SGD(model.parameters(), lr=lr)
    raise ValueError(f"{name} optimizer not recognized")


#%%
def good_grad(model):
    """ Check if the gradient of every parameters in a model is good (neither inf nor nan) """
    good_grad = True
    for param in model.parameters():
        if True in torch.isinf(param.grad) : good_grad = False
        if True in torch.isnan(param.grad) : good_grad = False
    return good_grad
        
def compute_PSNR(im1, im2, d=False):
    if not d : d = im1.max()
    mse = np.linalg.norm(im1  - im2)**2 / im1.size
    psnr = 10 * np.log10(d**2/mse)
    return psnr


#%% Fonciton seuillage
def TLT(x, c1, c2):
    """ trilinear treshold function : 0 if |x| < c1, x if |x| > c2, linear inbetween """
    y = torch.zeros_like(x)
    id_mask = (x.abs() >= c2)
    low_mask = (-c2 <= x) * (x <= -c1)
    high_mask = (c2 >= x) * (x >= c1)
    y = y + (c2/(c2 - c1) * x + (c1 * c2)/(c2 - c1)) * low_mask
    y = y + (c2/(c2 - c1) * x + (c1 * c2)/(c1 - c2)) * high_mask
    y = y + x * id_mask
    return y


#%%
def load(path, device = 'cpu', dtype=torch.float32): 
    """ load DeQuIP.params to a list of tensor """
    A = torch.load(path)
    b = [a.clone().to(device).detach() for a in A]
    return b
    
def idx_2x2(idx):
    if idx == 0 : return 0,0
    if idx == 1 : return 0,1
    if idx == 2 : return 1,0
    if idx == 3 : return 1,1
    
    
