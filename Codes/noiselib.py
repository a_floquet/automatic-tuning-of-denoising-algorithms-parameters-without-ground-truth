# -*- coding: utf-8 -*-
"""
@author: Arthur Floquet

File for adding noise to images
One-task functon are wrapped into more global function
"""

#%% Libs
import numpy as np
import torch
from math import sqrt

#%% Add single noise type to single image type, expressed in single strength metric
def gaussian_np_psnr(x, psnr, d, dtype, keep_noise):
    """ Noise is zero mean, d is the max data range"""
    x = x.astype(dtype)
    n = np.random.standard_normal(x.shape).astype(dtype)
    sigma = d * 10**(-psnr/20)
    y = x + sigma*n
    if not keep_noise : return y
    else : return y, sigma*n

def gaussian_np_snr(x, snr, dtype, keep_noise):
    """ Noise is zero mean"""
    x = x.astype(dtype)
    n = np.random.standard_normal(x.shape).astype(dtype)
    p_x = (x**2).mean()
    sigma = sqrt(p_x * 10**(-snr/10))
    y = x + sigma*n
    if not keep_noise : return y
    else : return y, sigma*n

def gaussian_torch_psnr(x, psnr, d, dtype, keep_noise):
    """ Noise is zero mean, d is the max data range"""
    x = x.type(dtype)
    n = torch.randn(x.shape, dtype = dtype, device = x.device)
    sigma = d * 10**(-psnr/20)
    y = x + sigma*n
    if not keep_noise : return y
    else : return y, sigma*n

def gaussian_torch_snr(x, snr, dtype, keep_noise):
    """ Noise is zero mean """
    dtype = x.dtype
    x = x.type(torch.float32)
    n = torch.randn(x.shape, dtype = dtype, device = x.device)
    p_x = (x**2).mean()
    sigma = sqrt(p_x * 10**(-snr/10))
    y = x + sigma*n
    if not keep_noise : return y
    else : return y, sigma*n

def poisson_np_psnr(x, psnr, d, dtype, keep_noise):
    """ d is max data range """
    x = x.astype(dtype)
    n = np.random.standard_normal(x.shape).astype(dtype) * np.sqrt(np.abs(x))
    p_n = (n**2).mean()
    n = (n / sqrt(p_n)) * sqrt((d**2) * 10**(-psnr/10))
    y = x + n
    if not keep_noise : return y
    else : return y, n

def poisson_np_snr(x, snr, dtype, keep_noise):
    x = x.astype(dtype)
    n = np.random.standard_normal(x.shape).astype(dtype) * np.sqrt(np.abs(x))
    p_x = (x**2).mean()
    p_n = (n**2).mean()
    n = (n / sqrt(p_n)) * sqrt((p_x**2) * 10**(-snr/10))
    y = x + n
    if not keep_noise : return y
    else : return y, n

def poisson_torch_psnr(x, psnr, d, dtype, keep_noise):
    """ d is max data range """
    x = x.type(dtype)
    n = torch.poisson(x.abs())
    p_n = (n**2).mean()
    a = sqrt((d**2 / p_n) * 10**(psnr/10))
    y = x + a*n
    if not keep_noise : return y
    else : return y, a*n

def poisson_torch_snr(x, snr, dtype, keep_noise):
    x = x.type(dtype)
    n = torch.poisson(x.abs())
    p_x = (x**2).mean()
    p_n = (n**2).mean()
    a = sqrt((p_x / p_n) * 10**(snr/10))
    y = x + a*n
    if not keep_noise : return y
    else : return y, a*n


#%% Add single noise type to single image type, expressed in any strength metric
def gaussian_np(x, strength, metric, d, dtype, keep_noise):
    if metric in ("psnr","PSNR") : 
        if d is None : raise TypeError("data range not specified for PSNR noise")
        return gaussian_np_psnr(x, strength, d, dtype, keep_noise)  
    if metric in ("snr","SNR") : return gaussian_np_snr(x, strength, dtype, keep_noise)    
    raise ValueError(f'{metric} metric not implemented')
    
def gaussian_torch(x, strength, metric, d, dtype, keep_noise):
    if metric in ("psnr","PSNR") : 
        if d is None : raise TypeError("data range not specified for PSNR noise")
        return gaussian_torch_psnr(x, strength, d, dtype, keep_noise) 
    if metric in ("snr","SNR") : return gaussian_torch_snr(x, strength, dtype, keep_noise) 
    raise ValueError(f'{metric} metric not implemented')
    
def poisson_np(x, strength, metric, d, dtype, keep_noise):
    if metric in ("psnr","PSNR") : 
        if d is None : raise TypeError("data range not specified for PSNR noise")
        return poisson_np_psnr(x, strength, d, dtype, keep_noise)
    if metric in ("snr","SNR") : return poisson_np_snr(x, strength, dtype, keep_noise)
    raise ValueError(f'{metric} metric not implemented')
    
def poisson_torch(x, strength, metric, d, dtype, keep_noise):
    if metric in ("psnr","PSNR") : 
        if d is None : raise TypeError("data range not specified for PSNR noise")
        return poisson_torch_psnr(x, strength, d, dtype, keep_noise) 
    if metric in ("snr","SNR") : return poisson_torch_snr(x, strength, dtype, keep_noise) 
    raise ValueError(f'{metric} metric not implemented')
    
#%% Add single noise type to any image type, expressed in any strength metric
def gaussian(x, strength, metric, d, dtype, keep_noise):
    if 'numpy' in str(type(x)): return gaussian_np(x, strength, metric, d, dtype, keep_noise)
    if 'torch' in str(type(x)): return gaussian_torch(x, strength, metric, d, dtype, keep_noise)
    raise TypeError(f'Cannot add noise to {type(x)} object')
    
def poisson(x, strength, metric, d, dtype, keep_noise):
    if 'numpy' in str(type(x)): return poisson_np(x, strength, metric, d, dtype, keep_noise)
    if 'torch' in str(type(x)): return poisson_torch(x, strength, metric, d, dtype, keep_noise)
    raise TypeError(f'Cannot add noise to {type(x)} object')
    
    
#%% Final wrapper
def add_noise(x, noise_type, strength, metric, dtype, d=None, keep_noise=False):
    if noise_type in ("Gaussian", "gaussian", "GAUSSIAN") : return gaussian(x,strength,metric, d, dtype, keep_noise)
    if noise_type in ("Poisson", "poisson", "GAUSSIAN")   : return poisson(x, strength, metric, d, dtype, keep_noise)
    raise ValueError(f'Noise type {noise_type} not implemented')
    
    
    
#%% Add poisson noise in another way - on NumPy array
def apply_poisson(x, lam):
    m, M = x.min(), x.max()
    x = (x - m) / (M - m)
    if 'numpy' in str(type(x)): y = np.random.poisson(lam * (x+0.5))/lam - 0.5
    else : y = torch.poisson(lam * (x+0.5))/lam - 0.5  
    y = y * (M - m) + m
    return y
    











