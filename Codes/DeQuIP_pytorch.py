# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 09:41:00 2022

@author: afloquet

De-QuIP algorithm in a PyTorch framework
https://arxiv.org/pdf/2108.13778.pdf
"""

#%% Imports
import torch
import torch.nn as nn
from torch.nn.parameter import Parameter
from math import ceil, prod
from utils import TLT

#%% Mother class
class DeQuIP_mother(nn.Module):
    """
    Mother class for other versions. 
    Parameters are not learnable and should be given in the forward pass.
    Soft thresholding, contribution-wise.
    """
    
    def __init__(self, patch_size, overlapping, search_window_size, *, patch_stride = None):
        """
        Parameters
        ----------
        patch_size : int or tuple/list of length 2
            if int, define square patch size.
            if tuple/list, define patch size along height and width.
            
        overlapping : int or tuple/list of length 2
            if int, define overlapping between patches.
            if tuple/list, define overlapping along height and width.
            for any direction, we must have : 0 <= overlapping < 1.
            
        search_window_size : int or tuple/list of length 2
            search window size in term of fully non-overlapping patches (e.g.
            size 1 = all patches up to the first one not overlapping will count
            size 1.5 = all patches up to the first one half overlapping those
            who do not overlap x)                                                           
            if int, define square search window size.
            if tuple/list, define rectangular search window size.
            for any direction, we must have : 1 < search_window_size.

        patch_stride : int or tuple/list of length 2, optionnal
            way to override the patch stride computation based on patch size +
            overlap.                
        """
        super(DeQuIP_mother,self).__init__()
        
        # Patch size
        if isinstance(patch_size,int) :self.patch_size = [patch_size, patch_size]
        elif len(patch_size)==2 : self.patch_size = list(patch_size)
        else : raise TypeError("given patch size is either not (int,tuple,list), or it has wrong length")
        
        # Stride between patches
        if patch_stride is not None :
            if isinstance(patch_stride, int): patch_stride = [patch_stride, patch_stride]
            elif not isinstance(patch_stride, (tuple,list)) : raise TypeError("given overlapping is either not (int,tuple,list)")
            if len(patch_stride) != 2 : raise ValueError("len of patch_stride is not 2")
            self.patch_stride = patch_stride
        else : 
            if isinstance(overlapping,(float,int)) : overlapping = [overlapping, overlapping]
            elif len(overlapping)==2 : overlapping = list(overlapping)
            else : raise TypeError("given overlapping is either not (int,tuple,list), or it has wrong length")
            if max(overlapping) >= 1 : raise ValueError("overlapping is >= 1 in at least one direction")
            if min(overlapping) < 0 : raise ValueError("overlapping is negative in at least one direction")
            self.patch_stride = [ceil((1-overlapping[i])*self.patch_size[i]) for i in range(2)]
        
        # Search window size
        if isinstance(search_window_size,(float,int)) :self.search_window_size = [search_window_size, search_window_size]
        elif len(search_window_size)==2 : self.search_window_size = list(search_window_size)
        else : raise TypeError("given search window size is either not (int,tuple,list), or it has wrong length")
        if min(self.search_window_size) < 1 : raise ValueError("search window size is <1 in at least one direction")
            
        
        
    def patchization(self,x):
        """
        Split input image x into patches according to patch size and stride.
        Generally the image is not divisible into k patches with s stride. When
        it is the case, some patches virtually extend beyond the image. Here we
        decide to keep patch size constant, and to simply put those patches at
        the border of the image. This was chosen for its simplicity, although 
        it introduce a non-constant overlapping between patches, as patches 
        near the end of the image have more overlap between them.
        
        Parameters
        ----------
        x : (N,C,H,W) tensor
            Image to split.

        Returns
        -------
        patches : (N,P,C,h,w) tensor
            The collection of P patches of size (C,h,w).
            
        """
        # Compute number of patches and create to-be-filled tensor
        N,C,H,W = x.shape
        self.P_h = ceil(1 + (H-self.patch_size[0])/self.patch_stride[0]) # number of patches along height
        self.P_w = ceil(1 + (W-self.patch_size[1])/self.patch_stride[1]) # number of patches along width 
        patches = torch.zeros((N, self.P_h*self.P_w, C, self.patch_size[0], self.patch_size[1])).type(x.type())
        
        self.H, self.W = H,W # useful for rebuilding x after
        self.patch_memory = dict() # keep in memory each patch location
        
        # Loop over all patches
        P_idx = 0 # idex of patch ij in the dimension P
        for i in range(self.P_h):
            for j in range(self.P_w):
                # Compute patch index limits for the 4 possible cases
                if i != self.P_h-1 and j != self.P_w -1: # patch is fully inside x
                    i_low, i_up = i*self.patch_stride[0], self.patch_size[0] + i*self.patch_stride[0]
                    j_low, j_up = j*self.patch_stride[1], self.patch_size[1] + j*self.patch_stride[1]
                elif i == self.P_h-1 and j != self.P_w-1: # patch overflows on the bottom border of x
                    i_low, i_up = H - self.patch_size[0], H
                    j_low, j_up = j*self.patch_stride[1], self.patch_size[1] + j*self.patch_stride[1]
                elif i != self.P_h-1 and j == self.P_w-1: # patch overflows on the right border of x
                    i_low, i_up = i*self.patch_stride[0], self.patch_size[0] + i*self.patch_stride[0]
                    j_low, j_up = W - self.patch_size[1], W
                else : # patch overflows on the bottom right corner of x
                    i_low, i_up = H - self.patch_size[0], H
                    j_low, j_up = W - self.patch_size[1], W
                # Fill final tensor with computed patch    
                p_ij = x[:, :, i_low:i_up, j_low:j_up]
                patches[:,P_idx, :, :, :] = p_ij
                self.patch_memory[P_idx] = [i_low,i_up,j_low,j_up]
                P_idx +=1
                                   
        return patches
    


    def interaction(self, x):
        """
        Computes the interaction map for each patch. It does so by rolling x
        over the patch dimension in all directions defining the neighborhood
        of a patch (below is the correspondance table between the spatial shift
        and the 1D patch dimension index shift):
            - left <=> -1
            - right <=> +1
            - up <=> -P_w (number of patches along width)
            - down <=> +P_w
            - up-left, up-right, down-left, down-right <=> +- P_w  +-1
        
        Some interactions patches are set to 0, e.g. the first patch has no up,
        left or up-left neighbors.        
        Each patch is interacting with its direct, potentially overlapping
        neighbors. 
        
        Parameters
        ----------
        x : (N,P,C,h,w) tensor
            Collection of patches.

        Returns
        -------
        I : (N,P,C,h,w) tensor
            Collection of interactions : I[n0,p0,:,:,:] correspond to the total
            interactions for patch x[n0,p0,:,:,:].

        """
        # Extract number of patches and create to-be-filled tensor
        I = torch.zeros_like(x)
        
        # Define shift : ceil(search_window_size*patch_size) = spatial shift. Then we
        # convert it to P dimension shift (see correspondance table above)
        dP = [self.P_w * ceil(self.search_window_size[0] * self.patch_size[0]/self.patch_stride[0]),# max x shift
              ceil(self.search_window_size[1] * self.patch_size[1]/self.patch_stride[1])] # max y shift
        dP_x = list(range(-dP[0],dP[0]+1,self.P_w)) # list of all x shifts
        dP_y = list(range(-dP[1],dP[1]+1)) # list of all y shifts
        dP_x.remove(0), dP_y.remove(0) # remove 0 from the lists       
        
        # Links a 1D shift along P dimension to the patches that will be set to 
        # 0, as they don't correspond to actual neighbors, e.g. -1 => left 
        # neighbor; the patches on the first columns don't have a left neighbor
        roll_idx_x = {i : self.get_rows_id(i) for i in dP_x}
        roll_idx_y = {j : self.get_cols_id(j) for j in dP_y}
        roll_idx_xy = {i+j : self.get_diags_id(i+j) for i in dP_x for j in dP_y}
        roll_idx = {**roll_idx_x, **roll_idx_y, **roll_idx_xy} # merge the dict
        
        # Loop over all directions
        for r in roll_idx :
            rolled = torch.roll(x, r ,1)
            # I_ab = |a - b|/d^2
            dy = r % self.P_w
            dx = (r - dy)/self.P_w
            d2 = (self.patch_stride[0]*dx)**2 + (self.patch_stride[1]*dy)**2 
            i = torch.abs(x-rolled) / d2
            i[:, roll_idx[r], :, :, :] = 0 # suppress non-real interactions
            I = I + i # I = sum of I_ab
        
        return I


          
    def basis(self,x , I, p, planck_factor):
        """
        Compute adaptative basis along with energy levels.

        Parameters
        ----------
        x : (N,P,C,h,w) tensor
            Collection of patches.
        I : (N,P,C,h,w) tensor
            Collection of interactions.
        p : scalar or (P) tensor
            Parameter of DeQuIP, proportioannlity constant for the interaction.
        planck_factor : scalar or (P) tensor
            Parameter of DeQuIP, defines behavior of waves w.r.t the potential.

        Returns
        -------
        E : (N,P,C,hw) tensor
            Energy levels for each patch.
        psi : (N,P,C,hw,hw) tensor
            Adaptative basis for each patch.

        """
        # Initialization
        N,P,C,h,w = x.shape
        if self.is_global(p, x.shape) : v = x + I * p.view(N,1,1,1,1) 
        else : v = x + I * p.view(N,P,1,1,1)
        if self.is_global(planck_factor, x.shape) : planck = planck_factor.view(N,1,1) * v.max(-1)[0].max(-1)[0] 
        else : planck = planck_factor.view(N,P,1) * v.max(-1)[0].max(-1)[0] 
        hw = h*w
        v_vectorized = v.reshape([N,P,C,hw])
        H = torch.zeros((N,P,C,hw,hw)).type(x.type())
        
        # Defines boundary conditions concerned indices
        planck_2 = [0,hw]
        planck_3 = [k * w for k in range(1,h)] + [(k*w) - 1 for k in range(1,h)]
        planck_0 = [k * w for k in range(1,h)] + [(k*w) - 1 for k in range(1,h)]
        
        # Fill hamiltonian with a loop (there is for sure a better way to do so)
        for i in range(hw):
            for j in range(hw):
                if i==j:
                    if   i in planck_2 : H[:,:,:,i,j] = v_vectorized[:,:,:,i] + 2*planck
                    elif i in planck_3 : H[:,:,:,i,j] = v_vectorized[:,:,:,i] + 3*planck
                    else : H[:,:,:,i,j] = v_vectorized[:,:,:,i] + 4*planck
                elif (i == j+1 or i == j-1) and (i not in planck_0 or j not in planck_0): H[:,:,:,i,j] = -planck                   
                elif i == j+w  or i == j-w : H[:,:,:,i,j] = -planck
    
        # Computation of eigenvectors and eigeinvalues
        device = H.device
        H = H.to('cpu')
        E, psi = torch.linalg.eigh(H)
        E, psi = E.to(device), psi.to(device)
        
        return E, psi
    

    
    def project_threshold_reproject(self,x, E, psi, c1, c2):
        """
        
        Project each patch onto its adaptative basis, threshold the projection,
        and project back onto image space.

        Parameters
        ----------
        x : (N,P,C,h,w) tensor
            Collection of patches.
        E : (N,P,C,hw) tensor
            Energy levels for each patch.
        psi : (N,P,C,hw,hw) tensor
            Adaptative basis for each patch.
        cutoff : scalar or (P) tensor
            Parameter of DeQuIP, cutoff value for the thresholding.
        slope : scalar or (P) tensor or str
            Parameter of DeQuIP, slope value for the thresholding.
            if 'hard', mimics a hard thresholding by having fixed value 100.

        Returns
        -------
        x : (N,P,C,h,w) tensor
            Collection of denoised patches.

        """        
        # Initialization
        N,P,C,h,w = x.shape
        x_vectorized = x.reshape(N,P,C,h*w)
        
        # Project coefficient
        proj_coeff = torch.matmul(x_vectorized.unsqueeze(3), psi)
        self.alpha = proj_coeff
        
        # Trilinear thresholding
        if self.is_global(c1, x.shape) : c1 = c1.view(N,1,1,1,1) 
        else : c1 = (c1.unsqueeze(-1) * torch.ones(h*w, dtype = x.dtype, device = x.device)).view(N,P,C,1, h*w)
        if self.is_global(c2, x.shape) : c2 = c2.view(N,1,1,1,1)
        else : c2 = (c2.unsqueeze(-1) * torch.ones(h*w, dtype = x.dtype, device = x.device)).view(N,P,C,1, h*w)

        thresholded_coeff = TLT(proj_coeff, c1, c2)
        
        # Reproject back to image space
        x_thresh_vect = torch.matmul(psi, thresholded_coeff.squeeze(3).unsqueeze(-1))
        x = x_thresh_vect.reshape(N,P,C,h,w)
        
        return x
        
    
    def unpatch(self,x):
        """
        Rebuild the full image from the collection of patches, by averaging 
        each overlapping patch.

        Parameters
        ----------
        x : (N,P,C,h,w) tensor
            Collection of patches.

        Returns
        -------
        x_denoised : (N,C,H,W) tensor
            Rebuilt image.

        """
        
        # Initialization
        N,P,C,h,w = x.shape
        normalization = torch.zeros(N, C, self.H, self.W).type(x.type())
        x_denoised = torch.zeros(N, C, self.H, self.W).type(x.type())
        
        # Loop over all patches
        for p in self.patch_memory:
            i_low,i_up, j_low,j_up = self.patch_memory[p]
            x_denoised[:,:, i_low:i_up, j_low:j_up] = x_denoised[:,:, i_low:i_up, j_low:j_up] + x[:,p,:,:,:]
            normalization[:,:, i_low:i_up, j_low:j_up] = normalization[:,:, i_low:i_up, j_low:j_up] + 1
        
        # Normalize
        x_denoised = torch.div(x_denoised, normalization)
        
        return x_denoised
            
        
    def forward(self, x, p, planck_factor, c1, c2): 
        p, planck_factor, c1, c2 = self.float2torch([p, planck_factor, c1, c2], x.device)
        
        x = self.patchization(x)
        means = x.mean([-1,-2]).unsqueeze(-1).unsqueeze(-1)
        x = x - means
        I = self.interaction(x)
        E, psi = self.basis(x, I, p, planck_factor)
        x = self.project_threshold_reproject(x, E, psi, c1, c2)
        x = x + means
        x = self.unpatch(x) 
        
        return x
 
    
########################## Sub-methods  ######################################   
    def get_rows_id(self, shift):
        """ returns patches indexes to not consider given a vertical shift """
        dx = int(shift / self.P_w)
        up = dx < 0
        rows_id = list()
        for i in range(abs(dx)):
            if up : rows_id += list(range(i*self.P_w,(i+1)*self.P_w)) # first rows
            else  : rows_id += list(range( (self.P_h-i-1)*self.P_w, (self.P_h-i)*self.P_w )) # last rows
            
        return rows_id
    
    def get_cols_id(self, shift):
        """ returns patches indexes to not consider given a horizontal shift"""
        dy = shift
        left = dy < 0
        cols_id = list()
        for j in range(abs(dy)):
            if left : cols_id += [k*self.P_w + j for k in range(self.P_h)] # first cols
            else    : cols_id += [(k+1)*self.P_w - j - 1 for k in range(self.P_h)] # last cols
            
        return cols_id
            
    def get_diags_id(self,shift):
        """ returns patches indexes to not consider given a diagonal shift """
        dy = shift % self.P_w
        dx = int((shift - dy)/self.P_w)
        up = dx < 0
        left = dy < 0
        diags_id = list()
        for i in range(abs(dx)):
            if up : diags_id += list(range(i*self.P_w,(i+1)*self.P_w)) # first rows
            else  : diags_id += list(range( (self.P_h-i-1)*self.P_w, (self.P_h-i)*self.P_w )) # last rows
        for j in range(abs(dy)):
            if left : diags_id += [k*self.P_w + j for k in range(self.P_h)] # first cols
            else    : diags_id += [(k+1)*self.P_w - j -1 for k in range(self.P_h)] # last cols
        
        diags_id = list(set(diags_id)) # remove duplicates
        return diags_id

    def is_global(self, param, shape):
        """ determine given a parameter and the input shape if the parameter is
        defined patch-wise or globally """
        if param.ndim == 0 : return True
        if param.ndim == 1 and param.shape[0] == shape[0] : return True
        if param.ndim == 2 and param.shape[1] == 1 : return True
        return False
    
    def float2torch(self, args, device):
        return torch.tensor(args, dtype=torch.float32, device = device)
        
##############################################################################
    

#%% Trilinear threshold on contribution
class DeQuIPNet(DeQuIP_mother):
    """ 
    DeQuIP parameters are learnable. 
    """
    
    def __init__(self, patch_size, overlapping, search_window_size,
                     init_params, patch_wise = [False,False,False, False], im_size = None,
                     **kwargs):
            
        super(DeQuIPNet,self).__init__(patch_size, overlapping, search_window_size, **kwargs)
        if True in patch_wise :
            if im_size is None : raise TypeError("Patch-wise parameter given, but image size is not given, thus the number of patches cannot be computed")
            H,W = im_size
            P = (ceil( 1 + (H - self.patch_size[0]) / self.patch_stride[0])) * (ceil(1 + (W-self.patch_size[1]) / self.patch_stride[1])) # number of patches 
        
        if init_params[3] == 'hard' : init_params[3] = 100.    
        init_params = torch.tensor(init_params)
        self.p = Parameter(init_params[0], requires_grad=True) if not patch_wise[0] else Parameter(init_params[0].repeat(P), requires_grad=True)
        self.planck_factor = Parameter(init_params[1], requires_grad=True) if not patch_wise[1] else Parameter(init_params[1].repeat(P), requires_grad=True)
        self.c1 = Parameter(init_params[2], requires_grad=True) if not patch_wise[2] else Parameter(init_params[2].repeat(P), requires_grad=True)
        self.c2 = Parameter(init_params[3], requires_grad=True) if not patch_wise[3] else Parameter(init_params[3].repeat(P), requires_grad=True)
        self.params = [self.p, self.planck_factor, self.c1, self.c2]
    
    def forward(self, x):

        p = self.p
        planck_factor = self.planck_factor
        c1 = self.c1
        c2 = self.c2
        
        x = self.patchization(x)
        means = x.mean([-1,-2]).unsqueeze(-1).unsqueeze(-1)
        x = x - means
        I = self.interaction(x)
        E, psi = self.basis(x, I, p, planck_factor)
        x = self.project_threshold_reproject(x, E, psi, c1, c2)
        x = x + means
        x = self.unpatch(x) 
        
        return x