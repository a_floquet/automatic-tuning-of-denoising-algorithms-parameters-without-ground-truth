# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 11:31:06 2023
@author: afloquet
Single image Noisy-as-clean superfit : single pair of noisy images used
"""

#%% Libs
from os import mkdir
from os.path import join
import numpy as np
import torch
import matplotlib.pyplot as plt

import utils
import noiselib
from DeQuIP_pytorch import DeQuIPNet

#%% Settings
datadir = '../Data/Fitting_images'
clean_dir = join(datadir, 'clean')
IM_PATH = utils.GetAllFiles(clean_dir)
many_z = True
alpha = 0.5


# DeQuIP settings
patch_size = 7
overlap = 'useless'
patch_stride = 1
search_window_size = 2

# Noise settings
noise_type = "gaussian"
NOISE_STRENGTH = [15,17,20,22,25,27,30]
noise_metric = "psnr"

# Training settings
n_epochs = 100
lr_small = 0.05
lr_big = 1

for im_path in IM_PATH :
    for noise_strength in NOISE_STRENGTH :

        # Initial parameters
        init_params = [0.1, 2., 72., 216.]
        patch_wise = [False, False, False, False]
        
        # Paths
        im_name = im_path[-12:-4]
        z_type = 'many_z' if many_z else 'single_z'
        savedir = f'../Results/DeQuIP_fitting/_megacomparison/R2R/{im_name}_{noise_metric}{noise_strength}'
        
        # dtype
        np_dtype = np.float32
        torch_dtype = torch.cuda.FloatTensor
        
        #%% Initialization
        # Create saving dir
        mkdir(savedir)
        
        # Create & save noisy image
        x_np  = plt.imread(im_path) * 255.
        y_np  = np.load(join(datadir, f'noisy_{noise_strength}dB', f'{im_name}.npy'))
        _, n  = noiselib.add_noise(y_np, noise_type, noise_strength, noise_metric, dtype = np_dtype, d = 255., keep_noise=True)
        z1_np = y_np + alpha * n
        z2_np = y_np - n/alpha
        
        
        x  = torch.from_numpy(x_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)            
        y  = torch.from_numpy(y_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)
        z1 = torch.from_numpy(z1_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)
        z2 = torch.from_numpy(z2_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)
        
        
        # Build model and optimizer
        DeQuIP = DeQuIPNet(patch_size, overlap, search_window_size, init_params, patch_wise, x_np.shape, patch_stride = patch_stride).type(torch_dtype)
        optimizer = torch.optim.Adam([{'params' : (DeQuIP.p, DeQuIP.planck_factor), 'lr' : lr_small},
                                      {'params' : (DeQuIP.c1, DeQuIP.c2), 'lr' : lr_big}])
        criterion = torch.nn.MSELoss()
        
        # Denoise with initial parameters
        with torch.no_grad():
            init_out = DeQuIP(y)
            init_out_np = init_out.squeeze().detach().cpu().numpy()
            np.save(join(savedir,'out_init.npy'), init_out_np)
                
        #%% Fitting
        # Initialize history lists
        loss_h = list()
        params_h = np.zeros((4, n_epochs+1))
        grad_h = np.zeros((4, n_epochs))
        
        for i, param in enumerate(DeQuIP.params): params_h[i, 0] = param.item()
        
        for e in range(1, n_epochs+1):
            if many_z : 
                _, n  = noiselib.add_noise(y, noise_type, noise_strength, noise_metric, dtype = torch_dtype.dtype, d = 255., keep_noise=True)
                z1 = y + alpha * n
                z2 = y - n/alpha
                
            out = DeQuIP(z1)
            loss = criterion(out, z2)
            loss.backward()
            optimizer.step()
            
            for i, param in enumerate(DeQuIP.params):
                grad_h[i, e-1] = param.grad.item()
                params_h[i, e] = param.item()
            loss_h.append(loss.item())
        
            optimizer.zero_grad()
            
            out_np = out.squeeze().detach().cpu().numpy()
            np.save(join(savedir,f'out_{e}.npy'), out_np)
                
        
        #%% Create figures
        fig = plt.figure(dpi = 500)
        plt.plot(loss_h)
        plt.xlabel('iteration')
        plt.ylabel('loss')
        plt.title('Fitting DeQuIP')
        plt.savefig(join(savedir, 'loss.png'), dpi = 500)
        
        
        
        names = ["p", "planck", "$c_1$", "$c_2$"]
        colors = ["blue", "black", "red", "limegreen"]
        fig, ax = plt.subplots(2,2, dpi = 500)
        for idx in range(4) : 
            i,j = utils.idx_2x2(idx)
            ax[i,j].plot(params_h[idx, :], color = colors[idx])
            ax[i,j].set(xlabel = 'iterations', ylabel = names[idx])
        fig.suptitle('Parameters during fitting')
        fig.tight_layout()
        plt.savefig(join(savedir, 'params.png'))
        
        
        
        fig, ax = plt.subplots(2,2, dpi = 500)
        for idx in range(4) : 
            i,j = utils.idx_2x2(idx)
            ax[i,j].plot(grad_h[idx, :], color = colors[idx])
            ax[i,j].set(xlabel = 'iterations', ylabel = names[idx])
        fig.suptitle('Parameters gradients during fitting')
        fig.tight_layout()
        plt.savefig(join(savedir, 'grads.png'))
        
        
        
        with torch.no_grad() : 
            if many_z : 
                _, n = noiselib.add_noise(y, noise_type, noise_strength, noise_metric, dtype = torch_dtype.dtype, d = 255., keep_noise=True)
                z1 = y + alpha * n
            out_final = DeQuIP(z1)
            out_final_np = out_final.squeeze().detach().cpu().numpy()
            np.save(join(savedir, 'out_final.npy'),out_final_np)
        
        fig, ax = plt.subplots(1,4, dpi=500)
        
        ax[0].imshow(y_np, cmap='gray')
        ax[0].axis(False)
        ax[0].set_title('noisy')
        
        ax[1].imshow(init_out_np, cmap='gray')
        ax[1].axis(False)
        ax[1].set_title('before fitting')
        
        ax[2].imshow(out_final_np, cmap='gray')
        ax[2].axis(False)
        ax[2].set_title('after fitting')
        
        ax[3].imshow(x_np, cmap='gray')
        ax[3].axis(False)
        ax[3].set_title('clean')
        
        fig.tight_layout()
        plt.savefig(join(savedir,'images.png'))
        
        
        # Save results
        torch.save(DeQuIP.params, join(savedir, 'params_final.pt')) 
        np.save(join(savedir, 'params_history.npy'), params_h)
        np.save(join(savedir, 'grad_history.npy'), grad_h)
        np.save(join(savedir, 'loss.npy'), np.asarray(loss_h))

