# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 11:31:06 2023
@author: afloquet
Single image Noisier2Noise
"""

#%% Libs
from os.path import isdir
from os import mkdir
from os.path import join
import numpy as np
import torch
import matplotlib.pyplot as plt

import utils
import noiselib
from DeQuIP_pytorch import DeQuIPNet

#%% helper functions
def psnr_alpha(psnr, alpha):
    return psnr - 20*np.log10(alpha)


#%% Settings
datadir = '../Data/Fitting_images'
clean_dir = join(datadir, 'clean')
IM_PATH = utils.GetAllFiles(clean_dir)
many_z = True
alpha = 1

# DeQuIP settings
patch_size = 7
overlap = 'useless'
patch_stride = 1
search_window_size = 2

# Noise settings
noise_type = "gaussian"
NOISE_STRENGTH = [15,17,20,22,25,27,30]
noise_metric = "psnr"

# Training settings
n_epochs = 100
lr_small = 1
lr_big = 1

# Loop
for im_path in IM_PATH :
    for noise_strength in NOISE_STRENGTH :
        
        # Initial parameters
        init_params = [0.1, 2., 72., 216.]
        patch_wise = [False, False, False, False]
        
        # Paths
        im_name = im_path[-12:-4]
        savedir = f'../Results/DeQuIP_fitting/_megacomparison/Nr2N/{im_name}_{noise_metric}{noise_strength}'
            
        # dtype
        np_dtype = np.float32
        torch_dtype = torch.cuda.FloatTensor
        
        #%% Initialization
        # Create saving dir
        mkdir(savedir)
        
        # Create & save noisy image
        x_np = plt.imread(im_path) * 255.
        y_np = np.load(join(datadir, f'noisy_{noise_strength}dB', f'{im_name}.npy'))
        z_np = noiselib.add_noise(y_np, noise_type, psnr_alpha(noise_strength, alpha), noise_metric, dtype = np_dtype, d = 255.)
        
        
        x = torch.from_numpy(x_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)            
        y = torch.from_numpy(y_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)
        z = torch.from_numpy(z_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)
        
        
        # Build model and optimizer
        DeQuIP = DeQuIPNet(patch_size, overlap, search_window_size, init_params, patch_wise, x_np.shape, patch_stride = patch_stride).type(torch_dtype)
        optimizer = torch.optim.Adam([{'params' : (DeQuIP.p, DeQuIP.planck_factor), 'lr' : lr_small},
                                      {'params' : (DeQuIP.c1, DeQuIP.c2), 'lr' : lr_big}])
        criterion = torch.nn.MSELoss()
        
        # Denoise with initial parameters
        with torch.no_grad():
            out = DeQuIP(z)
            inf = ((1 + alpha**2)*out - z)/alpha**2
            init_inf_np = inf.squeeze().detach().cpu().numpy()
            np.save(join(savedir,'inference_init.npy'), init_inf_np)
                
        #%% Fitting
        # Initialize history lists
        loss_h = list()
        params_h = np.zeros((4, n_epochs+1))
        grad_h = np.zeros((4, n_epochs))
        
        for i, param in enumerate(DeQuIP.params): params_h[i, 0] = param.item()
        
        for e in range(1, n_epochs+1):
            print(f'Starting iteration {e}')
            if many_z : z = noiselib.add_noise(y, noise_type, psnr_alpha(noise_strength, alpha), noise_metric, dtype = torch_dtype.dtype, d = 255.)
            
            out = DeQuIP(z)
            loss = criterion(out, y)
            loss.backward()
            optimizer.step()
            
            for i, param in enumerate(DeQuIP.params):
                grad_h[i, e-1] = param.grad.item()
                params_h[i, e] = param.item()
            loss_h.append(loss.item())
        
            optimizer.zero_grad()
            
            with torch.no_grad():
                inf = ((1 + alpha**2)*out - z)/alpha**2
                inf_np = inf.squeeze().detach().cpu().numpy()
            if e == n_epochs : np.save(join(savedir,'inference_final.npy'), inf_np)
            else : np.save(join(savedir,f'inference_{e}.npy'), inf_np)
            
            out_np = out.squeeze().detach().cpu().numpy()
            np.save(join(savedir,f'out_{e}.npy'), out_np)
            
        # Alternatvie inference method
        alt_out = DeQuIP(y)
        alt_inf = ((1 + alpha**2)*alt_out - y)/alpha**2
        alt_inf_np = alt_inf.squeeze().detach().cpu().numpy()
        np.save(join(savedir,'alt_inference_final.npy'), alt_inf_np)

        #%% Create figures
        fig = plt.figure(dpi = 500)
        plt.plot(loss_h)
        plt.xlabel('iteration')
        plt.ylabel('loss')
        plt.title('Fitting DeQuIP')
        plt.savefig(join(savedir, 'loss.png'), dpi = 500)
        
        
        
        names = ["p", "planck", "$c_1$", "$c_2$"]
        colors = ["blue", "black", "red", "limegreen"]
        fig, ax = plt.subplots(2,2, dpi = 500)
        for idx in range(4) : 
            i,j = utils.idx_2x2(idx)
            ax[i,j].plot(params_h[idx, :], color = colors[idx])
            ax[i,j].set(xlabel = 'iterations', ylabel = names[idx])
        fig.suptitle('Parameters during fitting')
        fig.tight_layout()
        plt.savefig(join(savedir, 'params.png'))
        
        
        
        fig, ax = plt.subplots(2,2, dpi = 500)
        for idx in range(4) : 
            i,j = utils.idx_2x2(idx)
            ax[i,j].plot(grad_h[idx, :], color = colors[idx])
            ax[i,j].set(xlabel = 'iterations', ylabel = names[idx])
        fig.suptitle('Parameters gradients during fitting')
        fig.tight_layout()
        plt.savefig(join(savedir, 'grads.png'))
        
        
        
        with torch.no_grad() : 
            if many_z : z = noiselib.add_noise(y, noise_type, psnr_alpha(noise_strength, alpha), noise_metric, dtype = torch_dtype.dtype, d = 255.)
            out_final = DeQuIP(z)
            out_final_np = out_final.squeeze().detach().cpu().numpy()
            np.save(join(savedir, 'out_final.npy'),out_final_np)
        
        fig, ax = plt.subplots(1,4, dpi=500)
        
        ax[0].imshow(y_np, cmap='gray')
        ax[0].axis(False)
        ax[0].set_title('noisy')
        
        ax[1].imshow(init_inf_np, cmap='gray')
        ax[1].axis(False)
        ax[1].set_title('before fitting')
        
        ax[2].imshow(inf_np, cmap='gray')
        ax[2].axis(False)
        ax[2].set_title('after fitting')
        
        ax[3].imshow(x_np, cmap='gray')
        ax[3].axis(False)
        ax[3].set_title('clean')
        
        fig.tight_layout()
        plt.savefig(join(savedir,'images.png'))
        
        
        # Save results
        torch.save(DeQuIP.params, join(savedir, 'params_final.pt')) 
        np.save(join(savedir, 'params_history.npy'), params_h)
        np.save(join(savedir, 'grad_history.npy'), grad_h)
        np.save(join(savedir, 'loss.npy'), np.asarray(loss_h))

