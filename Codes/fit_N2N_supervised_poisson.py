# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 15:12:41 2023
@author: afloquet
"""

#%% Libs
from os import mkdir
from os.path import join
import numpy as np
import torch
import matplotlib.pyplot as plt

from utils import compute_PSNR
import utils
import noiselib
from DeQuIP_pytorch import DeQuIPNet

#%% Settings
save = True
superfit = True

# Path to image
im_path = '../Data/Fitting_images/clean/test_296.png'
im_name = im_path.split('/')[-1].split('.')[0]

# DeQuIP settings
patch_size = 7
overlap = 'useless'
patch_stride = 1
search_window_size = 2

# Noise settings
noise_type = "poisson"
noise_strength = 17
noise_metric = "psnr"

# Training settings
n_epochs = 100
lr_small = 0.05
lr_big = 5

# Initial parameters
c1 = -4.*noise_strength + 160. # PSNR first guess
c2 = 2.5*c1
init_params = [0.1, 2., c1, c2]
patch_wise = [False, False, False, False]

# Paths
fit_type = 'superfit' if superfit else 'fit'
savedir = f'../Results/DeQuIP_fitting/N2N_sup_comparison/{im_name}_{noise_type}_{noise_metric}{noise_strength}_stride{patch_stride}'

# dtype
np_dtype = np.float32
torch_dtype = torch.cuda.FloatTensor

#%% Load data
if save : mkdir(savedir)

# Create inputs and targets
x_np = plt.imread(im_path) * 255.
x    = torch.from_numpy(x_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)

inp_np = noiselib.apply_poisson(x_np, 100).astype(np_dtype)
tar_n2n_np = noiselib.apply_poisson(x_np, 100).astype(np_dtype)

inp        = torch.from_numpy(inp_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)
tar_n2n    = torch.from_numpy(tar_n2n_np).unsqueeze(0).unsqueeze(0).type(torch_dtype)
tar        = x

# Build DeQuIP and optimizer
DeQuIP = DeQuIPNet(patch_size, overlap, search_window_size, init_params, patch_wise, im_size = x_np.shape, patch_stride = patch_stride).type(torch_dtype)
optimizer = torch.optim.Adam([{'params' : (DeQuIP.p, DeQuIP.planck_factor), 'lr' : lr_small} , {'params' : (DeQuIP.c1, DeQuIP.c2), 'lr' : lr_big}])

DeQuIP_n2n = DeQuIPNet(patch_size, overlap, search_window_size, init_params, patch_wise, im_size = x_np.shape, patch_stride = patch_stride).type(torch_dtype)
optimizer_n2n = torch.optim.Adam([{'params' : (DeQuIP_n2n.p, DeQuIP_n2n.planck_factor), 'lr' : lr_small} , {'params' : (DeQuIP_n2n.c1, DeQuIP_n2n.c2), 'lr' : lr_big}])

criterion     = torch.nn.MSELoss()

# Denoising with initial parameters
with torch.no_grad():
    out = DeQuIP(inp)
    out_init_np = out.squeeze().detach().cpu().numpy()
    if save : np.save(join(savedir,'out_init.npy'), out_init_np)
    
#%% Fitting
loss_sup = list()
loss_n2n = list()
psnr_sup = list()
psnr_n2n = list()

for e in range(1, n_epochs+1):
    # Recreate input / target if neeeded
    if not superfit : 
        inp     = noiselib.add_noise(x, noise_type, noise_strength, noise_metric, dtype = torch_dtype.dtype, d = 255.)
        tar_n2n = noiselib.add_noise(x, noise_type, noise_strength, noise_metric, dtype = torch_dtype.dtype, d = 255.)
            
    # Supervised training
    out = DeQuIP(inp)
    loss = criterion(tar, out)
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()    

    loss_sup.append(loss.item())

    out_np = out.squeeze().cpu().detach().numpy()
    psnr = compute_PSNR(x_np, out_np, d=255.)
    psnr_sup.append(psnr)
    if save : np.save(join(savedir,f'out_sup_{e}.npy'), out_np)
        
    # Supervised training
    out_n2n = DeQuIP_n2n(inp)
    loss_n2n_ = criterion(tar_n2n, out_n2n)
    loss_n2n_.backward()
    optimizer_n2n.step()
    optimizer_n2n.zero_grad()    

    loss_n2n.append(loss_n2n_.item())
    
    out_np = out_n2n.squeeze().cpu().detach().numpy()
    psnr = compute_PSNR(x_np, out_np, d=255.)
    psnr_n2n.append(psnr)
    if save : np.save(join(savedir,f'out_n2n_{e}.npy'), out_np)
        
# Get last output (after last weights update)
with torch.no_grad():           
    out_final = DeQuIP(inp)
    out_final_np = out_final.squeeze().cpu().numpy()
    if save : np.save(join(savedir,'out_sup_final.npy'),out_final_np)
    
    out_final_n2n = DeQuIP_n2n(inp)
    out_final_np_n2n = out_final_n2n.squeeze().cpu().numpy()
    if save : np.save(join(savedir,'out_n2n_final.npy'),out_final_np_n2n)
    
        
#%% Plots
# Loss
fig = plt.figure(dpi=500)
plt.plot(loss_sup, label='supervised')
plt.plot(loss_n2n, label='Noise2Noise')
plt.xlabel('iterations')
plt.ylabel('loss')
plt.legend(loc='upper right')
plt.title(f'Loss during DeQuIP {fit_type}')
if save : plt.savefig(join(savedir,'loss.png'))
plt.show()

#♣ PSNR
fig = plt.figure(dpi=500)
plt.plot(psnr_sup, label='supervised')
plt.plot(psnr_n2n, ls='--', label='Noise2Noise')
plt.xlabel('iterations k')
plt.ylabel('$PSNR(x, \hat{x}_k)$')
plt.legend(loc='upper right')
plt.title(f'output PSNR during DeQuIP {fit_type}')
if save : plt.savefig(join(savedir,'psnr.png'))
plt.show()

# Images
im_dict = {'noisy'  : inp.squeeze().detach().cpu().numpy(),
           'init'   : out_init_np,
           'final_n2n' : out_final_np_n2n,
           'final'  : out_final_np,
           'clean'  : x_np}

fig, ax = plt.subplots(1,5,dpi=500)
for i, title in enumerate(im_dict) : 
    ax[i].imshow(im_dict[title], cmap='gray')
    ax[i].axis(False)
    ax[i].set_title(title)
    
fig.tight_layout()
if save : plt.savefig(join(savedir, 'images.png'))
plt.show()


if save : 
    torch.save(DeQuIP.params    , join(savedir, 'params_sup_final.pt'))
    torch.save(DeQuIP_n2n.params, join(savedir, 'params_n2n_final.pt'))
    np.save(join(savedir, 'loss_sup.npy'), np.asarray(loss_sup))
    np.save(join(savedir, 'loss_n2n.npy'), np.asarray(loss_n2n))
   
            
