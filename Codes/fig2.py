# -*- coding: utf-8 -*-
"""
Created on Mon Jul  3 18:00:10 2023

@author: afloquet
"""
#%% Libs
import os
from os import listdir
from os.path import join
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import container
from matplotlib.lines import Line2D
import noiselib
from utils import compute_PSNR
import torch

#%% Paths and experiment settings
motherdir = '../../Results/DeQuIP_fitting/_megacomparison'
datadir = '../../Data/Fitting_images'
cleandir = join(datadir, 'clean')
STRENGTH = [15,17,20,22,25,27,30]
METHOD = {'supervised' : 'out_100.npy', 
          'N2N' : 'out_100.npy',
          'NaC' : 'inference_final.npy',
          'Nr2N': 'inference_final.npy', 
          'R2R' : 'montecarlo_inference_final.npy',
          'SC_metric_lotofnan' : 'out_100.npy'} #,
          # 'init' : 'out_init.npy'}

PARAM  = ['supervised', 'N2N', 'NaC', 'Nr2N', 'R2R', 'SC_metric_lotofnan']

#%% Get PSNR 
big_PSNR = np.zeros((35,7,6))
for im, im_name in enumerate(listdir(cleandir)):
    im_name = im_name[:-4]
    
    full_path = join(cleandir, f'{im_name}.png')
    x = plt.imread(full_path) * 255.
    
    
    for s, strength in enumerate(STRENGTH) : 
        y_path = join(datadir, f'noisy_{strength}dB', f'{im_name}.npy')
        y = np.load(y_path)
        
        for m, method in enumerate(METHOD):
            if method == 'init' : denoised_path = join(motherdir, 'supervised', f'{im_name}_psnr{strength}', METHOD['init'])
            else : denoised_path = join(motherdir, method, f'{im_name}_psnr{strength}', METHOD[method])
            if method == "SC_metric_lotofnan" and os.path.isfile(join(motherdir, "SC_metric",f'{im_name}_psnr{strength}', METHOD[method])):
                denoised_path = join(motherdir, "SC_metric",f'{im_name}_psnr{strength}', METHOD[method])
            denoised = np.load(denoised_path)    
            
            psnr = compute_PSNR(x, denoised, d=255.)
            big_PSNR[im, s, m] = psnr
                

mean_psnr = np.nanmean(big_PSNR, axis = 0)
std_psnr  = np.nanstd(big_PSNR, axis  = 0) 
        


#%% Plot results
fig = plt.figure(dpi = 500)

line_sup  = plt.plot(STRENGTH, mean_psnr[:,0], ls='--', label = '$\mathbf{x}^{*}$', color='black')
line_n2n  = plt.plot(STRENGTH, mean_psnr[:,1], label = '$\mathbf{\hat{x}}^{\mathrm{N2N}}$')
line_nac  = plt.plot(STRENGTH, mean_psnr[:,2], label = '$\mathbf{\hat{x}}^{\mathrm{NaC}}$')
line_nr2n = plt.plot(STRENGTH, mean_psnr[:,3], label = '$\mathbf{\hat{x}}^{\mathrm{Nr2N}}$')
line_r2r  = plt.plot(STRENGTH, mean_psnr[:,4], label = '$\mathbf{\hat{x}}^{\mathrm{R2R}}$')
line_sc   = plt.plot(STRENGTH, mean_psnr[:,5], label = '$\mathbf{\hat{x}}^{\mathrm{NRSS}}$')
# line_init = plt.plot(STRENGTH, mean_psnr[:,5], label = '$\mathbf{\hat{x}}_0$')


plt.errorbar(STRENGTH, mean_psnr[:,1], yerr = std_psnr[:,1], color = line_n2n[0].get_color(), capsize=5)
plt.errorbar(STRENGTH, mean_psnr[:,2], yerr = std_psnr[:,2], color = line_nac[0].get_color(), capsize=5)
plt.errorbar(STRENGTH, mean_psnr[:,3], yerr = std_psnr[:,3], color = line_nr2n[0].get_color(), capsize=5)
plt.errorbar(STRENGTH, mean_psnr[:,4], yerr = std_psnr[:,4], color = line_r2r[0].get_color(), capsize=5)
plt.errorbar(STRENGTH, mean_psnr[:,5], yerr = std_psnr[:,5], color = line_sc[0].get_color(), capsize=5)
plt.errorbar(STRENGTH, mean_psnr[:,0], yerr = std_psnr[:,0], color = line_sup[0].get_color(), ls='--', capsize=5)
# plt.errorbar(STRENGTH, mean_psnr[:,5], yerr = std_psnr[:,5], color = line_init[0].get_color(), ls='--', capsize=5)

plt.grid(ls='dotted')
plt.xlabel('Input PSNR (dB)')
plt.ylabel('Output PSNR (dB) - mean & std.')


plt.legend(loc = 'upper left')

plt.show()
